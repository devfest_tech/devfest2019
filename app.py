from flask import Flask, jsonify, request
from node import NODE


app = Flask(__name__)
map_warning = {
    "1" : "Đường phố thông thoáng",
    "2" : "Phương tiện lưu thông trung bình",
    "3" : "Phương tiện lưu thông đông, có nguy cơ tắc đường",
    "4" : "Phương tiện lưu thông rất đông, đang tắc đường"
}

@app.route('/')
def hello_world():
    return 'API Infinity Traffic'

@app.route("/apiNodes/<articeid>", methods=["GET", "OPTION"])
def add_node(articeid):
    if articeid is None:
        articeid = "0"
    #  tim now
    add_node_lst_n = []
    idx_1 = ["1", "2", "4", "3"]
    node_1 = NODE("1", "Nguyễn Văn Linh - Đường 2/9", (16.061031, 108.223909), idx_1[0], map_warning[idx_1[0]])
    node_2 = NODE("2", "Võ Văn Kiệt - Ngô QUyền", (16.061382, 108.234342), idx_1[1], map_warning[idx_1[1]])
    node_3 = NODE("3", "Nguyễn Văn Thoại - Ngũ Hành Sơn", (16.053181, 108.236876), idx_1[2], map_warning[idx_1[2]])
    node_4 = NODE("4", "Duy Tân - Đường 2/9", (16.049421, 108.222337), idx_1[3], map_warning[idx_1[3]])
    add_node_lst_n.append(node_1.node_name())
    add_node_lst_n.append(node_2.node_name())
    add_node_lst_n.append(node_3.node_name())
    add_node_lst_n.append(node_4.node_name())

    #  tim 1h
    add_node_lst_1 = []
    idx_2 = ["2", "4", "4", "3"]
    node_1 = NODE("1", "Nguyễn Văn Linh - Đường 2/9", (16.061031, 108.223909), idx_2[0], map_warning[idx_2[0]])
    node_2 = NODE("2", "Võ Văn Kiệt - Ngô QUyền", (16.061382, 108.234342), idx_2[1], map_warning[idx_2[1]])
    node_3 = NODE("3", "Nguyễn Văn Thoại - Ngũ Hành Sơn", (16.053181, 108.236876), idx_2[2], map_warning[idx_2[2]])
    node_4 = NODE("4", "Duy Tân - Đường 2/9", (16.049421, 108.222337), idx_2[3], map_warning[idx_2[3]])
    add_node_lst_1.append(node_1.node_name())
    add_node_lst_1.append(node_2.node_name())
    add_node_lst_1.append(node_3.node_name())
    add_node_lst_1.append(node_4.node_name())

    #  tim 2h
    add_node_lst_2 = []
    idx_3 = ["4", "1", "4", "3"]
    node_1 = NODE("1", "Nguyễn Văn Linh - Đường 2/9", (16.061031, 108.223909), idx_3[0], map_warning[idx_3[0]])
    node_2 = NODE("2", "Võ Văn Kiệt - Ngô QUyền", (16.061382, 108.234342), idx_3[1], map_warning[idx_3[1]])
    node_3 = NODE("3", "Nguyễn Văn Thoại - Ngũ Hành Sơn", (16.053181, 108.236876), idx_3[2], map_warning[idx_3[2]])
    node_4 = NODE("4", "Duy Tân - Đường 2/9", (16.049421, 108.222337), idx_3[3], map_warning[idx_3[3]])
    add_node_lst_2.append(node_1.node_name())
    add_node_lst_2.append(node_2.node_name())
    add_node_lst_2.append(node_3.node_name())
    add_node_lst_2.append(node_4.node_name())

    #  tim 3h
    add_node_lst_3 = []
    idx_4 = ["4", "4", "4", "3"]
    node_1 = NODE("1", "Nguyễn Văn Linh - Đường 2/9", (16.061031, 108.223909), idx_4[0], map_warning[idx_4[0]])
    node_2 = NODE("2", "Võ Văn Kiệt - Ngô QUyền", (16.061382, 108.234342), idx_4[1], map_warning[idx_4[1]])
    node_3 = NODE("3", "Nguyễn Văn Thoại - Ngũ Hành Sơn", (16.053181, 108.236876), idx_4[2], map_warning[idx_4[2]])
    node_4 = NODE("4", "Duy Tân - Đường 2/9", (16.049421, 108.222337), idx_4[3], map_warning[idx_4[3]])
    add_node_lst_3.append(node_1.node_name())
    add_node_lst_3.append(node_2.node_name())
    add_node_lst_3.append(node_3.node_name())
    add_node_lst_3.append(node_4.node_name())

    #  tim 4h
    add_node_lst_4 = []
    idx_5 = ["1", "4", "2", "3"]
    node_1 = NODE("1", "Nguyễn Văn Linh - Đường 2/9", (16.061031, 108.223909), idx_5[0], map_warning[idx_5[0]])
    node_2 = NODE("2", "Võ Văn Kiệt - Ngô QUyền", (16.061382, 108.234342), idx_5[1], map_warning[idx_5[1]])
    node_3 = NODE("3", "Nguyễn Văn Thoại - Ngũ Hành Sơn", (16.053181, 108.236876), idx_5[2], map_warning[idx_5[2]])
    node_4 = NODE("4", "Duy Tân - Đường 2/9", (16.049421, 108.222337), idx_5[3], map_warning[idx_5[3]])
    add_node_lst_4.append(node_1.node_name())
    add_node_lst_4.append(node_2.node_name())
    add_node_lst_4.append(node_3.node_name())
    add_node_lst_4.append(node_4.node_name())

    choose = {
        "0" : add_node_lst_n,
        "1" : add_node_lst_1,
        "2" : add_node_lst_2,
        "3" : add_node_lst_3,
        "4" : add_node_lst_4,
    }
    print(choose[articeid])
    response = {
        "status" : 200,
        "data" : choose[articeid],
        "message" : "success",
        "error" : 0
    }
    return jsonify(response), 200


@app.route("/apiStressOpt", methods=["POST"])
def stress_opt():
    data = request.get_json(force=True)
    # data_format = {
    #     "start_a" : "",
    #     "position_a" : "",
    #     "finish_b" : "",
    #     "position_b" : "",
    #     "type" : ""
    # }
    print(data)

@app.after_request
def add_headers(response):
    response.headers.add('Content-Type', 'application/json')
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS')
    response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
    response.headers.add('Access-Control-Expose-Headers', 'Content-Type,Content-Length,Authorization,X-Pagination')
    return response


if __name__ == '__main__':
    from argparse import ArgumentParser

    parser = ArgumentParser()
    parser.add_argument('-p', '--port', default=5000, type=int, help='port to listen on')
    args = parser.parse_args()
    port = args.port
    app.run(host='0.0.0.0', port=5000)

