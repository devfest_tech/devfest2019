### Overview

#### Vietnam currently ranks among the top 10 most air-polluting countries in Asia. 
#### The reasons come from many aspects: construction works that are not covered, industrial development like thermal power, iron, steel... And especially the high level of vehicle on streets.
#### This leads to hours traffic jam in most cities and make people deal with difficulties in daily life: going to work late, facing with health problems like lung cancer, , dry throat, headache.
#### To solve the above issue, our team - Infinity Traffic ................
#### Let's see what our team can do

#

![](./Architecture/infinity.png)

## Detail Guide

### Deep learning based object detection using the YOLOv3 algorithm and object recognition and tracking using the Deepsort algorithm, the end using image proccessing.

## Object detection
### Get started
The YOLOv3 (You Only Look Once) is a state-of-the-art, real-time object detection algorithm. The published model recognizes 80 different objects in images and videos. For more details, you can refer to this paper.
![](./Architecture/yolov3.png)

## Object Regconition and tracking

This repository contains code for Simple Online and Realtime Tracking with a Deep Association Metric (Deep SORT). We extend the original SORT algorithm to integrate appearance information based on a deep appearance descriptor. See the arXiv preprint for more information.
![](./Architecture/deepsort.png)

## Recommendation
A recommender system is a simple algorithm whose aim is to provide the most relevant information to a user by discovering patterns in a dataset. The algorithm rates the items and shows the user the items that they would rate highly. An example of recommendation in action is when you visit Amazon and you notice that some items are being recommended to you or when Netflix recommends certain movies to you. They are also used by Music streaming applications such as Spotify and Deezer to recommend music that you might like.
![](./Architecture/recomend.png)

# Used to
## Requirements

It requires Python 3.6 to run our system

Development for this project will be isolated in Python virtual environment. This allows us to experiment with different versions of dependencies.

There are many ways to install `virtual environment (virtualenv)`, see the [Python Virtual Environments: A Primer](https://realpython.com/python-virtual-environments-a-primer/) guide for different platforms:

```bash
$ pip install virtualenv
```


###Result
Image:

![](./Architecture/result1.png)
![](./Architecture/result2.png)
