from flask import jsonify
class NODE():
    def __init__(self, id, name, position, alertlevel, reason):
        self.id = id
        self.name = name
        self.position = position
        self.alertlevel = alertlevel
        self.reason = reason

    def node_name(self):
        return {
            "id": self.id,
            "name": self.name,
            "alertlevel": self.alertlevel,
            "position":
                {
                    "lat" : self.position[0],
                    "lng" : self.position[1]
                },
            "reason": self.reason
        }

if __name__ == '__main__':

    node_1 = NODE("1", "Nút giao thông số 01", (16.0507611, 108.2093622), "1", 0)
    print(node_1.node_name())