import cv2
import numpy as np
import tensorflow as tf
from PIL import Image

def postprocess_boxes(pred_bbox, org_img_shape, input_size, score_threshold):

    valid_scale=[0, np.inf]
    pred_bbox = np.array(pred_bbox)

    pred_xywh = pred_bbox[:, 0:4]
    pred_conf = pred_bbox[:, 4]
    pred_prob = pred_bbox[:, 5:]

    # # (1) (x, y, w, h) --> (xmin, ymin, xmax, ymax)
    pred_coor = np.concatenate([pred_xywh[:, :2] - pred_xywh[:, 2:] * 0.5,
                                pred_xywh[:, :2] + pred_xywh[:, 2:] * 0.5], axis=-1)
    # # (2) (xmin, ymin, xmax, ymax) -> (xmin_org, ymin_org, xmax_org, ymax_org)
    org_h, org_w = org_img_shape
    resize_ratio = min(input_size / org_w, input_size / org_h)

    dw = (input_size - resize_ratio * org_w) / 2
    dh = (input_size - resize_ratio * org_h) / 2

    pred_coor[:, 0::2] = 1.0 * (pred_coor[:, 0::2] - dw) / resize_ratio
    pred_coor[:, 1::2] = 1.0 * (pred_coor[:, 1::2] - dh) / resize_ratio

    # # (3) clip some boxes those are out of range
    pred_coor = np.concatenate([np.maximum(pred_coor[:, :2], [0, 0]),
                                np.minimum(pred_coor[:, 2:], [org_w - 1, org_h - 1])], axis=-1)
    invalid_mask = np.logical_or((pred_coor[:, 0] > pred_coor[:, 2]), (pred_coor[:, 1] > pred_coor[:, 3]))
    pred_coor[invalid_mask] = 0

    # # (4) discard some invalid boxes
    bboxes_scale = np.sqrt(np.multiply.reduce(pred_coor[:, 2:4] - pred_coor[:, 0:2], axis=-1))
    scale_mask = np.logical_and((valid_scale[0] < bboxes_scale), (bboxes_scale < valid_scale[1]))

    # # (5) discard some boxes with low scores
    classes = np.argmax(pred_prob, axis=-1)
    scores = pred_conf * pred_prob[np.arange(len(pred_coor)), classes]
    score_mask = scores > score_threshold
    mask = np.logical_and(scale_mask, score_mask)
    coors, scores, classes = pred_coor[mask], scores[mask], classes[mask]

    return np.concatenate([coors, scores[:, np.newaxis], classes[:, np.newaxis]], axis=-1)

def image_preporcess(image, target_size, gt_boxes=None):

    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB).astype(np.float32)

    ih, iw    = target_size
    h,  w, _  = image.shape

    scale = min(iw/w, ih/h)
    nw, nh  = int(scale * w), int(scale * h)
    image_resized = cv2.resize(image, (nw, nh))

    image_paded = np.full(shape=[ih, iw, 3], fill_value=128.0)
    dw, dh = (iw - nw) // 2, (ih-nh) // 2
    image_paded[dh:nh+dh, dw:nw+dw, :] = image_resized
    image_paded = image_paded / 255.

    if gt_boxes is None:
        return image_paded

    else:
        gt_boxes[:, [0, 2]] = gt_boxes[:, [0, 2]] * scale + dw
        gt_boxes[:, [1, 3]] = gt_boxes[:, [1, 3]] * scale + dh
        return image_paded, gt_boxes

def read_pb_return_tensors(graph, pb_file, return_elements):

    with tf.gfile.FastGFile(pb_file, 'rb') as f:
        frozen_graph_def = tf.GraphDef()
        frozen_graph_def.ParseFromString(f.read())

    with graph.as_default():
        return_elements = tf.import_graph_def(frozen_graph_def,
                                              return_elements=return_elements)
    return return_elements


def bboxes_iou(boxes1, boxes2):
    boxes1 = np.array(boxes1)
    boxes2 = np.array(boxes2)

    boxes1_area = (boxes1[..., 2] - boxes1[..., 0]) * (boxes1[..., 3] - boxes1[..., 1])
    boxes2_area = (boxes2[..., 2] - boxes2[..., 0]) * (boxes2[..., 3] - boxes2[..., 1])

    left_up = np.maximum(boxes1[..., :2], boxes2[..., :2])
    right_down = np.minimum(boxes1[..., 2:], boxes2[..., 2:])

    inter_section = np.maximum(right_down - left_up, 0.0)
    inter_area = inter_section[..., 0] * inter_section[..., 1]
    union_area = boxes1_area + boxes2_area - inter_area
    ious = np.maximum(1.0 * inter_area / union_area, np.finfo(np.float32).eps)

    return ious


def nms(bboxes, iou_threshold, sigma=0.3, method='nms'):
    """
    :param bboxes: (xmin, ymin, xmax, ymax, score, class)

    Note: soft-nms, https://arxiv.org/pdf/1704.04503.pdf
          https://github.com/bharatsingh430/soft-nms
    """
    classes_in_img = list(set(bboxes[:, 5]))
    target_classes = [2, 3, 4, 5, 6, 7, 8]
    best_bboxes = []
    #     print(classes_in_img)
    for cls in classes_in_img:
        if int(cls) in target_classes:

            cls_mask = (bboxes[:, 5] == cls)
            cls_bboxes = bboxes[cls_mask]

            while len(cls_bboxes) > 0:
                max_ind = np.argmax(cls_bboxes[:, 4])
                best_bbox = cls_bboxes[max_ind]
                best_bboxes.append(best_bbox)
                cls_bboxes = np.concatenate([cls_bboxes[: max_ind], cls_bboxes[max_ind + 1:]])
                iou = bboxes_iou(best_bbox[np.newaxis, :4], cls_bboxes[:, :4])
                weight = np.ones((len(iou),), dtype=np.float32)

                assert method in ['nms', 'soft-nms']

                if method == 'nms':
                    iou_mask = iou > iou_threshold
                    weight[iou_mask] = 0.0

                if method == 'soft-nms':
                    weight = np.exp(-(1.0 * iou ** 2 / sigma))

                cls_bboxes[:, 4] = cls_bboxes[:, 4] * weight
                score_mask = cls_bboxes[:, 4] > 0.
                cls_bboxes = cls_bboxes[score_mask]

    return best_bboxes

def calculate_scale(y, s):
    alpha = 1.5

    if y >= 240:
        s = s + s*(1/(alpha*abs(160 - y)/240))
    else:
        s = s + s*(alpha*abs(160-y)/240)

    return s


def draw_bbox(image, bboxes, show_label=True):
    """
    bboxes: [x_min, y_min, x_max, y_max, probability, cls_id] format coordinates.
    """
    image_h, image_w, _ = image.shape

    classes = ['person', 'bicycle', 'car', 'motorcycle', 'airplane',
               'bus', 'train', 'truck', 'boat', 'traffic light', 'fire hydrant',
               'stop sign', 'parking meter', '']
    scale = 0
    tmp_image = np.zeros((480, 640), dtype=np.uint8)

    for i, bbox in enumerate(bboxes):
        coor = np.array(bbox[:4], dtype=np.int32)
        fontScale = 0.5
        score = bbox[4]
        class_ind = int(bbox[5])
        bbox_color = (255, 0, 0)
        bbox_thick = int(0.6 * (image_h + image_w) / 600)
        c1, c2 = (coor[0], coor[1]), (coor[2], coor[3])
        ss = abs(coor[0] - coor[2]) * abs(coor[1] - coor[3])
        ss = calculate_scale(coor[1], ss)
        scale += ss
        cv2.rectangle(image, c1, c2, bbox_color, bbox_thick)

        if show_label:
            #             print(classes[class_ind])
            bbox_mess = '%s: %.2f' % (classes[class_ind], score)
            t_size = cv2.getTextSize(bbox_mess, 0, fontScale, thickness=bbox_thick // 2)[0]
            cv2.rectangle(image, c1, (c1[0] + t_size[0], c1[1] - t_size[1] - 3), bbox_color, -1)  # filled

            cv2.putText(image, bbox_mess, (c1[0], c1[1] - 2), cv2.FONT_HERSHEY_SIMPLEX,
                        fontScale, (0, 0, 0), bbox_thick // 2, lineType=cv2.LINE_AA)
        cv2.rectangle(tmp_image,c1, c2,(255),-1 )
    sss = scale/(image_h*image_w)

    return image, tmp_image

class Van_Yolo():

    def __init__(self):
        self.graph = tf.Graph()
        return_elements = ["input/input_data:0", "pred_sbbox/concat_2:0", "pred_mbbox/concat_2:0",
                           "pred_lbbox/concat_2:0"]
        pb_file = '/home/van/work/devfest/deep_sort_yolov3-master/deep_sort_yolov3-master/model_data/yolov3_coco.pb'
        self.return_tensors = read_pb_return_tensors(self.graph, pb_file, return_elements)
        self.yolo_sess = tf.Session(graph=self.graph)

    def  detect_image(self, original_image):
        original_image = cv2.cvtColor(original_image, cv2.COLOR_BGR2RGB)

        original_image_size = original_image.shape[:2]
        image_data = image_preporcess(np.copy(original_image), [416, 416])
        image_data = np.expand_dims(image_data, axis=0)
        num_classes = 80

        pred_sbbox, pred_mbbox, pred_lbbox = self.yolo_sess.run(
            [self.return_tensors[1], self.return_tensors[2], self.return_tensors[3]],
            feed_dict={self.return_tensors[0]: image_data})

        pred_bbox = np.concatenate([np.reshape(pred_sbbox, (-1, 5 + num_classes)),
                                    np.reshape(pred_mbbox, (-1, 5 + num_classes)),
                                    np.reshape(pred_lbbox, (-1, 5 + num_classes))], axis=0)
        bboxes = postprocess_boxes(pred_bbox, original_image_size, 416, 0.3)
        bboxes = nms(bboxes, 0.25, method='nms')
        image, tmp_image = draw_bbox(original_image, bboxes)

        return image, tmp_image


test = Van_Yolo()
video_path = ''
video_capture = cv2.VideoCapture('video_input/high4.mp4')
out = cv2.VideoWriter('output_video/high4_output.mp4', cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'), 15, (640, 480))
def points_to_poly(points, w, h, color):
    label = np.zeros((h, w), dtype='uint8')
    image_tmp = np.zeros((h, w, 3), dtype='uint8')
    array = np.array(points)
    array = np.expand_dims(array, axis=1)
    cv2.fillPoly(label, [array], 90)
    if color == 'green':
        cv2.fillPoly(image_tmp, [array], (0, 255, 0))
    elif color == 'red':
        cv2.fillPoly(image_tmp, [array], (0, 0, 255))
    else:
        cv2.fillPoly(image_tmp, [array], (0, 255, 255))
    return label, image_tmp
def find_contour(mask):
    contours, _ = cv2.findContours(mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    return contours

def combine_two_by_mask(image1, image2, mask):
    if len(mask.shape) == 2:
        alpha = cv2.merge((mask, mask, mask))
    else:
        alpha = np.copy(mask)
    alpha = alpha / 255.0
    fg = cv2.multiply(image1.astype(float), alpha)
    bg = cv2.multiply(image2.astype(float), (1.0 - alpha))
    img = cv2.add(fg, bg)
    return img.astype(np.uint8)
p1 = [(1, 250), (219, 310), (1, 447), (504, 479), (534, 354), (626, 360),(637, 270), (573, 245), (614, 98), (529, 105), (345, 230), (7, 170)]
p2 = [(2, 431), (242, 190), (390, 191), (631, 442)]
p3 = [(264, 219), (6, 290), (7, 467), (633, 470), (560, 225)]
p4 = [(134, 461), (626, 431), (622, 106), (351, 127), (134, 7), (7, 8)]
def combine_neg(mask_ori, mask_car):
    mask_car = mask_car/255.0
    mask_intersection = mask_ori*mask_car
    c1 = find_contour(np.uint8(mask_intersection))
    aa = 0
    for c in c1:
        aa += cv2.contourArea(c)
    return aa/area
while True:

    ret, frame = video_capture.read()  # frame shape 640*480*3
    frame = cv2.resize(frame, (640, 480))
    if ret != True:
        break
    image, tmp_image = test.detect_image(frame)
    mask, _ = points_to_poly(p4, 640, 480, 'red')
    cc = find_contour(mask)
    area = cv2.contourArea(cc[0])

    sss = combine_neg(mask, tmp_image)
    if sss < 0.45:
        _, mask_color = points_to_poly(p4, 640, 480, 'green')
    elif sss>=0.45 and sss<0.75:
        _, mask_color = points_to_poly(p4, 640, 480, 'yellow')
    else:
        _, mask_color = points_to_poly(p4, 640, 480, 'red')
    final = combine_two_by_mask(mask_color, image, mask)

    str_scale = "{0:.0f}%".format(sss*100)
    cv2.putText(final, str_scale, (50, 50), cv2.FONT_HERSHEY_COMPLEX, 1.0, (255, 255, 0), 2)
    out.write(final)
    cv2.imshow('', final)
    cv2.waitKey(1)

video_capture.release()
out.release()

